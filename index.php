<?php

require __DIR__ . '/vendor/autoload.php';

use HG\Consumer\Analytics;
use HG\Consumer\Consumers\StreamConsumer;

$streamConsumer = new StreamConsumer('bbb@gmail.com', 'Denis Ritchie');
$analytics = new Analytics($streamConsumer);
$analytics->init();

$analytics->averageCharLenPerMonth();
$analytics->longestPostCharLenPerMonth();
$analytics->totalPostsWeeknumber();
