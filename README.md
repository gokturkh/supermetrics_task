# Supermetrics API Consumer
> A simple API consumer & analytics tool.

Gets token & posts data from endpoint using different ways and prints analytics.

## Installation
Create posts table(posts.sql)
```sh
composer install
```

## Usage example
```php
$streamConsumer = new StreamConsumer('bbb@gmail.com', 'Denis Ritchie');
$analytics = new Analytics($streamConsumer);
$analytics->init(); // Get token & data from API endpoints and put into local DB.

$analytics->averageCharLenPerMonth();
$analytics->longestPostCharLenPerMonth();
$analytics->totalPostsWeeknumber();
```

## Meta

Hayrettin Gokturk – hayrettin.gokturk@gmail.com