<?php

namespace HG\Consumer\Consumers;

use HG\Consumer\Consumer;
use HG\Consumer\ConsumerInterface;

/**
 * Class CurlConsumer
 *
 * Gets data from API endpoint using curl.
 *
 * @package HG\Consumer\Consumers
 */
class CurlConsumer extends Consumer implements ConsumerInterface
{
    /**
     * CurlConsumer constructor.
     *
     * @param  string  $email
     * @param  string  $name
     */
    public function __construct(string $email, string $name)
    {
        $this->setEmail($email);
        $this->setName($name);
        $this->data = array();
    }

    /**
     * Gets posts from posts endpoint by page.
     *
     * @param  string  $token
     * @param  int     $page
     *
     * @return string
     */
    protected function getPostsFromApi(string $token, int $page): string
    {
        // TODO: Implement getPostsFromApi() method.
    }

    /**
     * Gets token from token endpoint.
     *
     * @return string
     */
    public function getTokenFromApi(): string
    {
        // TODO: Implement getTokenFromApi() method.
    }

    /**
     * Gets 10 pages of posts from posts endpoint.
     *
     * @param  string  $token
     *
     * @return string
     */
    public function getAllPosts(string $token): string
    {
        // TODO: Implement getAllPosts() method.
    }
}
