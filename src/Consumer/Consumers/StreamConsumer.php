<?php

namespace HG\Consumer\Consumers;

use HG\Consumer\Consumer;
use HG\Consumer\ConsumerInterface;

/**
 * Class StreamConsumer
 *
 * Gets data from API endpoint using stream context.
 *
 * @package HG\Consumer\Consumers
 */
class StreamConsumer extends Consumer implements ConsumerInterface
{
    /**
     * StreamConsumer constructor.
     *
     * @param  string  $email
     * @param  string  $name
     */
    public function __construct(string $email, string $name)
    {
        $this->setEmail($email);
        $this->setName($name);
        $this->data = array();
    }

    /**
     * Gets token from token endpoint.
     *
     * @return string
     */
    public function getTokenFromApi(): string
    {
        echo 'Getting token...' . PHP_EOL;
        $params = ['client_id' => $this::CLIENT_ID, 'email' => $this->getEmail(), 'name' => $this->getName()];

        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($params)
            )
        );

        $context = stream_context_create($options);
        $result = @file_get_contents(parent::TOKEN_ENDPOINT, false, $context);

        if ($result === false) {
            echo 'Can not get token: ' . error_get_last()['message'] . PHP_EOL;
            return null;
        }

        echo 'Token received.' . PHP_EOL;

        $result = json_decode($result);
        return json_encode($result->data->sl_token);
    }

    /**
     * Gets 10 pages of posts from posts endpoint.
     *
     * @param  string  $token
     *
     * @return string
     */
    public function getAllPosts(string $token): string
    {
        for ($i = 1; $i <= 10; $i++) {
            $temp_data = $this->getPostsFromApi($token, $i);
            if ($temp_data !== null) {
                $this->data = array_merge($this->data, json_decode($temp_data));
            }
        }
        if (empty($this->data)) {
            echo 'No post received ' . PHP_EOL;
            return null;
        }
        echo 'Posts received ' . PHP_EOL;
        echo 'Count: ' . count($this->data) . PHP_EOL;
        return json_encode($this->data);
    }

    /**
     * Gets posts from posts endpoint by page.
     *
     * @param  string  $token
     * @param  int     $page
     *
     * @return string
     */
    protected function getPostsFromApi(string $token, int $page): string
    {
        echo 'Getting posts page: ' . $page . '...' . PHP_EOL;
        $params = ['sl_token' => $token, 'page' => $page];

        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'GET',
            )
        );
        $context = stream_context_create($options);
        $result = @file_get_contents(
            sprintf(
                "%s?sl_token=%s&page=%s",
                $this::DATA_ENDPOINT,
                $params['sl_token'],
                $params['page']
            ),
            false,
            $context
        );
        if ($result === false) {
            echo 'Can not get data: ' . error_get_last()['message'] . PHP_EOL;
            return null;
        }
        $result = json_decode($result);

        return json_encode($result->data->posts);
    }
}
