<?php

namespace HG\Consumer;

/**
 * Class Analytics
 *
 * @package HG\Consumer
 */
class Analytics
{
    /**
     * @var \HG\Consumer\ConsumerInterface
     */
    private ConsumerInterface $consumer;

    /**
     * Analytics constructor.
     *
     * @param  \HG\Consumer\ConsumerInterface  $consumerInterface
     */
    public function __construct(ConsumerInterface $consumerInterface)
    {
        $this->consumer = $consumerInterface;
    }

    /**
     * Get token from API endpoint
     * Get posts from post API endpoint with token and put into db.
     *
     * @throws \Exception
     */
    public function init(): void
    {
        $token = $this->consumer->getTokenFromApi();

        if ($token !== null) {
            $data = $this->consumer->getAllPosts($token);

            if ($data !== null) {
                echo 'Inserting into database...' . PHP_EOL;
                DB::getInstance()->insertData($data);
                echo 'Insert complete.' . PHP_EOL;
            }
        }
    }

    /**
     * Print average char length per month.
     */
    public function averageCharLenPerMonth(): void
    {
        $data = DB::getInstance()->averageCharLenPerMonth();

        if (empty($data)) {
            echo 'No average character length of posts per month data.' . PHP_EOL;
        } else {
            echo 'Average character length of posts per month: ' . PHP_EOL;
            foreach ($data as $elem) {
                number_format((float)$elem['avg_length'], 2, '.', '');

                echo 'AVG: ' . number_format(
                    (float)$elem['avg_length'],
                    2,
                    '.',
                    ''
                ) . '    |    Date: ' . $elem['date'] . PHP_EOL;
            }
        }
    }

    /**
     * Print longest post char length per month.
     */
    public function longestPostCharLenPerMonth(): void
    {
        $data = DB::getInstance()->longestPostCharLenPerMonth();

        if (empty($data)) {
            echo 'No longest post by character length per month data.' . PHP_EOL;
        } else {
            echo 'Longest post by character length per month: ' . PHP_EOL;
            foreach ($data as $elem) {
                echo 'Length: ' . $elem['len'] . '    |    Date: ' . $elem['date'] . PHP_EOL;
            }
        }
    }

    /**
     * Print total posts split by week number.
     */
    public function totalPostsWeeknumber(): void
    {
        $data = DB::getInstance()->totalPostsWeeknumber();

        if (empty($data)) {
            echo 'No total posts split by week number data.' . PHP_EOL;
        } else {
            echo 'Total posts split by week number: ' . PHP_EOL;
            foreach ($data as $elem) {
                echo 'Count: ' . $elem['count'] . '    |    Week: ' . $elem['week'] . PHP_EOL;
            }
        }
    }
}
