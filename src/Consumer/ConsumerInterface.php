<?php

namespace HG\Consumer;

/**
 * Interface ConsumerInterface
 *
 * @package HG\Consumer
 */
interface ConsumerInterface
{
    /**
     * @return string
     */
    public function getTokenFromApi(): string;

    /**
     * @param  string  $token
     *
     * @return string
     */
    public function getAllPosts(string $token): string;
}
