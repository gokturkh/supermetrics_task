<?php

namespace HG\Consumer;

use DateTime;
use mysqli;

/**
 * Class Db
 *
 * Singleton DB Class
 *
 * @package HG\Consumer
 */
final class Db
{
    /**
     * Singleton instance.
     *
     * @var null
     */
    private static $instance = null;
    /**
     * Host address.
     *
     * @var string
     */
    private string $host = '192.168.10.10';
    /**
     * Database name.
     *
     * @var string
     */
    private string $dbname = 'homestead';
    /**
     * Username
     *
     * @var string
     */
    private string $username = 'homestead';
    /**
     * Password
     *
     * @var string
     */
    private string $password = 'secret';
    /**
     * Connection.
     *
     * @var \mysqli|null
     */
    private $conn = null;

    /**
     * Db constructor.
     */
    private function __construct()
    {
        $this->conn = new mysqli($this->host, $this->username, $this->password, $this->dbname);

        if ($this->conn->connect_errno) {
            exit();
        }
    }

    /**
     * Return DB singleton.
     *
     * @return \HG\Consumer\Db|null
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Db();
        }

        return self::$instance;
    }

    /* public function getConnection()
    {
        return $this->conn;
    } */

    /**
     * Insert the posts data got with API call.
     *
     * @param  string  $data
     *
     * @throws \Exception
     */
    public function insertData(string $data)
    {
        $data = json_decode($data);

        if (
            !($stmt = $this->conn->prepare(
                "INSERT INTO posts (name, user_id, message, type, created_time) VALUES (?,?,?,?,?)"
            ))
        ) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
        }

        if (!$stmt->bind_param("sisss", $name, $userId, $message, $type, $createdTime)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        foreach ($data as $row) {
            $name = $row->from_name;
            $userId = intval(substr($row->from_id, strpos($row->from_id, "_") + 1));
            $message = $row->message;
            $type = $row->type;

            $timestamp = strtotime($row->created_time);
            $dt = new DateTime('@' . $timestamp);

            $createdTime = $dt->format('Y-m-d H:i:s');

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
        }
    }

    /**
     * Get average char length per month.
     *
     * @return array
     */
    public function averageCharLenPerMonth(): array
    {
        /*
         * TODO: Disable ONLY_FULL_GROUP_BY from MySql Settings.
         *
         * https://stackoverflow.com/a/36033983/1240167
         */
        if (
            !($stmt = $this->conn->prepare(
                "SELECT AVG(CHAR_LENGTH(message)) avg_length, DATE_FORMAT(created_time,'%Y-%m') AS" .
                " date FROM posts GROUP BY YEAR(created_time), MONTH(created_time);"
            ))
        ) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $result = $stmt->get_result();

        // TODO: Maybe put the code below into an utility class.

        $res = array();
        while ($row = $result->fetch_assoc()) {
            array_push($res, $row);
        }

        return $res;
    }

    /**
     * Get longest post char length per month.
     *
     * @return array
     */
    public function longestPostCharLenPerMonth(): array
    {
        if (
            !($stmt = $this->conn->prepare(
                "SELECT MAX(CHAR_LENGTH(message)) as len, DATE_FORMAT(created_time,'%Y-%m') AS" .
                " date FROM posts GROUP BY YEAR(created_time), MONTH(created_time);"
            ))
        ) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $result = $stmt->get_result();

        // TODO: Maybe put the code below into an utility class.

        $res = array();
        while ($row = $result->fetch_assoc()) {
            array_push($res, $row);
        }

        return $res;
    }

    /**
     * Get total posts split by week number.
     *
     * @return array
     */
    public function totalPostsWeeknumber(): array
    {
        if (
            !($stmt = $this->conn->prepare(
                "SELECT COUNT(id) as count, WEEK(created_time) as week from posts GROUP BY WEEK(created_time);"
            ))
        ) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $result = $stmt->get_result();

        // TODO: Maybe put the code below into an utility class.

        $res = array();
        while ($row = $result->fetch_assoc()) {
            array_push($res, $row);
        }

        return $res;
    }
}
