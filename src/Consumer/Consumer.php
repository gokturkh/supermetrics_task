<?php

namespace HG\Consumer;

/**
 * Class Consumer
 *
 * @package HG\Consumer
 */
abstract class Consumer
{
    /**
     * The client ID.
     *
     * @var string
     */
    protected const CLIENT_ID = 'ju16a6m81mhid5ue1z3v2g0uh';
    /**
     * The token endpoint.
     *
     * @var string
     */
    protected const TOKEN_ENDPOINT = 'https://api.supermetrics.com/assignment/register';
    /**
     * The data endpoint.
     *
     * @var string
     */
    protected const DATA_ENDPOINT = 'https://api.supermetrics.com/assignment/posts';

    /**
     * Email.
     *
     * @var string
     */
    private string $email;
    /**
     * Name.
     *
     * @var string
     */
    private string $name;
    /**
     * Posts data returns from the data endpoint.
     *
     * @var array
     */
    protected array $data;

    /**
     * Email getter.
     *
     * @return string
     */
    protected function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Email setter.
     *
     * @param  string  $email
     */
    protected function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * Name getter.
     *
     * @return string
     */
    protected function getName(): string
    {
        return $this->name;
    }

    /**
     * Name setter.
     *
     * @param  string  $name
     */
    protected function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Gets posts from posts endpoint by page.
     *
     * @param  string  $token
     * @param  int     $page
     *
     * @return string
     */
    abstract protected function getPostsFromApi(string $token, int $page): string;
}
